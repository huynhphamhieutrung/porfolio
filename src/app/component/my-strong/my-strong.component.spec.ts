import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyStrongComponent } from './my-strong.component';

describe('MyStrongComponent', () => {
  let component: MyStrongComponent;
  let fixture: ComponentFixture<MyStrongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyStrongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyStrongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
