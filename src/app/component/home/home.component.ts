import { Component, OnInit } from '@angular/core';
// @ts-ignore
import Typewriter from 't-writer.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const writer = new Typewriter(document.querySelector('.name'), {
      loop: true,
      typeColor: 'white'
    });

    writer
      .strings(
        400,
        'Backend Developer',
        'Fullstack Developer'
      )
      .start();
  }
}
