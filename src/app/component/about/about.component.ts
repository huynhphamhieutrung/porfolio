import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  mobile = false;
  constructor() { }

  ngOnInit(): void {
    if (window.screen.width < 480) { // 768px portrait
      this.mobile = true;
    }
  }

}
