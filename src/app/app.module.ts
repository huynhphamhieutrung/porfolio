import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AngularFullpageModule} from '@fullpage/angular-fullpage';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './component/home/home.component';
import {AboutComponent} from './component/about/about.component';
import {ResumeComponent} from './component/resume/resume.component';
import {ContactComponent} from './component/contact/contact.component';
import {HeaderComponent} from './component/header/header.component';
import {ExperienceComponent} from './component/experience/experience.component';
import {ActivityComponent} from './component/activity/activity.component';
import {MyStrongComponent} from './component/my-strong/my-strong.component';
import {Resume2Component} from './component/resume2/resume2.component';
import {Resume3Component} from './component/resume3/resume3.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ResumeComponent,
    ContactComponent,
    HeaderComponent,
    ExperienceComponent,
    ActivityComponent,
    MyStrongComponent,
    Resume2Component,
    Resume3Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFullpageModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
