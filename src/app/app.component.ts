import {Component} from '@angular/core';
// @ts-ignore
import { options, fullpage_api } from 'fullpage.js/dist/fullpage.extensions.min';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  config: options;

  constructor() {
    this.config = {
      navigation: true,
      anchors: ['home', 'about', 'novagroup', 'officience', 'freelancer', 'Skills', 'contact'],
      navigationTooltips:  ['Home', 'About', 'Novagroup', 'Officience', 'Freelancer', 'Skills', 'Contact'],
      showActiveTooltip: false,
      scrollingSpeed: 1000,
      controlArrows: false,
      slidesNavigation: true,
    };
  }
}
